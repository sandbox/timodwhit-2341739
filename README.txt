/**
 * ShareThis Field
 */

The purpose of this module is to create a simple extra field on displays where
wanted. It pulls in the block data and will give you essentially the block as a
field to move around in the display.

To Use:
  - Enable the module
  - Go to a node type edit page
  - Select "Show field" under "ShareThis Field" Settings

This module requires ShareThis.
